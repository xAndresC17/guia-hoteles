
    $(function () {
        $("[data-toggle='tooltip']").tooltip();
        $("[data-toggle='popover']").popover();
        $('.carousel').carousel({
          interval: 2000
        });
  
        var contact = null;
  
        /*Mientras se esta mostrando*/
        $('#contacto').on('show.bs.modal', function (e) {
          console.log('se esta mostrando');
          contact = e.relatedTarget.id;
          $('#' + contact).prop('disabled', true);
          $('#' + contact).removeClass('btn-outline-success');
          $('#' + contact).addClass('btn-primary');
          console.log('btn deshabilitado');
        });
  
        /*cuanto termina de mostrar*/
        $('#contacto').on('shown.bs.modal', function (e) {
          console.log('finalizo de mostrar');
        });
  
        /*mientras se esta ocultando*/
        $('#contacto').on('hide.bs.modal', function (e) {
          console.log('se esta ocultando');
          $('#' + contact).prop('disabled', false);
          $('#' + contact).addClass('btn-outline-success');
          $('#' + contact).removeClass('btn-primary');
          contact = null
          console.log('btn habilitado');
        });
  
        $('#contacto').on('hidden.bs.modal', function (e) {
          console.log('Se termino de ocultar');
        });
  
      })