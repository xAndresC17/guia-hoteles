'use strict'

var gulp = require('gulp'),
    sass = require('gulp-sass'),
    del = require('del'),
    browserSync = require('browser-sync'),
    imagemin = require('gulp-imagemin'),
    uglify = require('gulp-uglify'),
    usemin = require('gulp-usemin'),
    rev = require('gulp-rev'),
    cleanCss = require('gulp-clean-css'),
    flatmap = require('gulp-flatmap'),
    htmlmin = require('gulp-htmlmin');

gulp.task('sass', function (done) {
    gulp.src('./css/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./css'));
    done()
});

gulp.task('sass:watch', function (done) {
    gulp.watch('./css/*.scss', gulp.series('sass'));
    done()
});

gulp.task('browser-sync', function (done) {
    var files = ['./*.html', './css/*.css', './img/*.{png, jpg, gif}', './js/*.js']
    browserSync.init(files, {
        server: {
            baseDir: './'
        }
    });
    done()
});

gulp.task('default', gulp.series('browser-sync', 'sass:watch'));


gulp.task('clean', function (done) {
    return del(['dist']);
    done()
});

gulp.task('copyfonts', function (done) {
    gulp.src('./node_modules/open-iconic/font/fonts/*.*')
        .pipe(gulp.dest('./dist/fonts'));
    done()
});

gulp.task('imagemin', function (done) {
    return gulp.src('./images/*')
        .pipe(imagemin({
            optimizationLevel: 3,
            progressive: true,
            interlaced: true
        }))
        .pipe(gulp.dest('dist/images'))
    done()
});

gulp.task('usemin', function (done) {

    return gulp.src('./*.html')
        .pipe(flatmap(function (stream, file) {
            return stream
                .pipe(usemin({
                    css: [rev()],
                    html: [function () {
                        return htmlmin({
                            collapseWhitespace: true
                        })
                    }],
                    js: [uglify(), rev()],
                    inlinejs: [uglify()],
                    inlinecss: [cleanCss(), 'concat']
                }));
        }))
        .pipe(gulp.dest('dist/'));
    done()
});

gulp.task('build', gulp.series('clean', 'copyfonts', 'imagemin', 'usemin'));